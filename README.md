# Writeup Fortnite CTF 2020

Writeup Fortnite CTF 2020 yang diadakan oleh NetSOS SIG - Ristek Fasilkom Universitas Indonesia.

## Problem List

### Binary Exploitation
| Problem | Points |
|:---|:---:|
| [AAAAAAAAAAAAA](binary-exploitation/200-aaaaaaaaaaaaa/README.md) | 200 |
| [Format String Easy](binary-exploitation/200-format-string-easy/README.md) | 200 |
| [Pacil Rich Bank](binary-exploitation/200-pacil-rich-bank/README.md) | 200 |


### Cryptography
| Problem | Points |
|:---|:---:|
| [Encoding EZ](cryptography/100-encoding-ez/README.md) | 100 |
| [Matematika Diskret](cryptography/100-matematika-diskret/README.md) | 100 |
| [RSA-1](cryptography/100-rsa-1/README.md) | 100 |
| [W](cryptography/100-w/README.md) | 100 |
| [COVID19 Remainder Theorem](cryptography/200-covid19-remainder-theorem/README.md) | 200 |


### Forensic
| Problem | Points |
|:---|:---:|
| [Di Depan Mata](forensic/100-di-depan-mata/README.md) | 100 |
| [Phonecall](forensic/200-phonecall/README.md) | 200 |


### Misc
| Problem | Points |
|:---|:---:|
| [Sanity Check](misc/1-sanity-check/README.md) | 1 |
| [Discord Spam](misc/100-discord-spam/README.md) | 100 |
| [Drawing Pad](misc/200-drawing-pad/README.md) | 200 |
| [Ranting](misc/200-ranting/README.md) | 200 |
| [Echooo](misc/300-echooo/README.md) | 300 |


### Reverse Engineering
| Problem | Points |
|:---|:---:|
| [crackmeme1](reverse-engineering/100-crackmeme1/README.md) | 100 |
| [random](reverse-engineering/100-random/README.md) | 100 |
| [crackmeme2](reverse-engineering/200-crackmeme2/README.md) | 200 |
| [crackmeme3](reverse-engineering/300-crackmeme3/README.md) | 300 |


### Web
| Problem | Points |
|:---|:---:|
| [Classic PHP](web/100-classic-php/README.md) | 100 |
| [Ariq CTF](web/200-ariq-ctf/README.md) | 200 |
| [PaciLingo](web/300-pacilingo/README.md) | 300 |


## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md).